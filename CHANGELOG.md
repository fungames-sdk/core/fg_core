# CHANGELOG

## Version 3.3.0.1 (19/07/2024)

### Updated

* Remove "InitializationStart" and "InitializationComplete" events, and replace it with "SDKLoadComplete" when FG is fully initialized

## Version 3.3.0.0 (01/01/2024)

### Updated

* Update architecture for 3.3

# Analytics

To access FG Analytics Scripting API and documentation :

- <a href="fg_analytics.html" target="_blank">Analytics</a>

To add an implementation of FG Analytics please refer to the corresponding section :

- <a href="fg_game_analytics.html" target="_blank">Game Analytics</a>
- <a href="fg_firebase.html" target="_blank">Firebase Analytics</a>
- <a href="fg_flurry.html" target="_blank">Flurry Analytics</a>

# MMP

To access FG MMP Scripting API and documentation :

- <a href="fg_mmp.html" target="_blank">MMP</a>

To add an implementation of FG MMP please refer to the corresponding section :

- <a href="fg_adjust.html" target="_blank">Adjust</a>

# User Consent

To access FG UserConsent Scripting API and documentation :

- <a href="fg_user_consent.html" target="_blank">UserConsent</a>

## ATT

To add an implementation of FG ATT please refer to the corresponding section :

- <a href="fg_att.html" target="_blank">ATT</a>
- <a href="fg_unity_att.html" target="_blank">Unity ATT</a>

## ATT Pre-Popup (Optional)

To add an implementation of FG ATT PrePopup please refer to the corresponding section :

- <a href="fg_att_prepopup.html" target="_blank"> ATT Pre-Popup</a>
- <a href="fg_custom_att_prepopup.html" target="_blank">Custom ATT Pre-Popup</a>

## GDPR

To add an implementation of FG GDPR please refer to the corresponding section :

- <a href="fg_gdpr.html" target="_blank">GDPR</a>
- <a href="fg_custom_gdpr.html" target="_blank">Custom GDPR</a>
- <a href="fg_google_cmp.html" target="_blank">Google CMP</a>

# Monetization

Monetization regroups different categories of modules used to monetize your game : Ad Mediation, Audio Ads, In-Game Ads,
Crosspromo, IAPs.

## Mediation

To access FG Mediation Scripting API and documentation :

- <a href="fg_mediation.html" target="_blank">Mediation</a>

In FunGames SDK, we currently use Applovin Max to handle Ad mediation. Please refer to the section below to integrate it
into your project:

- <a href="fg_applovin_max.html" target="_blank">Applovin Max</a>

If using Amazon as a network for Applovin Mediation, you will also need to import the AmazonWithMax FG module as
described below :

- <a href="3.2_Monetization_Mediation_AmazonWithMax.html" target="_blank">Amazon with Max</a>

## Audio Ads

To access FG AudioAds Scripting API and documentation :

- <a href="fg_audio_ads.html" target="_blank">AudioAds</a>

To add an implementation of FG AudioAds please refer to the corresponding section :

- <a href="fg_audio_mob.html" target="_blank">AudioMob</a>
- <a href="fg_odeeo" target="_blank">Odeeo</a>
- <a href="fg_gadsme_audio" target="_blank">Gadsme Audio</a>

## In-Game Ads

To access FG InGame Ads Scripting API and documentation :

- <a href="fg_in_game_ads.html" target="_blank">In-Game Ads</a>

To add an implementation of FG InGameAds please refer to the corresponding section :

- <a href="fg_in_game_ads.html" target="_blank">Adverty</a>
- <a href="fg_anzu.html" target="_blank">Anzu</a>
- <a href="fg_gadsme.html" target="_blank">Gadsme</a>
- <a href="fg_google_native.html" target="_blank">Google Native</a>
- <a href="fg_google_immersive_ads.html" target="_blank">Google Immersive</a>

## Crosspromo

To access FG InGame Ads Scripting API and documentation :

- <a href="fg_crosspromo.html" target="_blank">Crosspromo</a>

To add an implementation of FG Crosspromo please refer to the corresponding section :

- <a href="fg_tn_crosspromo.html" target="_blank">TapNation Crosspromo</a>

## IAP

To access FG IAP Scripting API and documentation :

- <a href="fg_iap.html" target="_blank">In App Purchases</a>

To add an implementation of FG IAP please refer to the corresponding section :

- <a href="fg_unity_iap.html" target="_blank">Unity IAP</a>

If you want to track IAPs, please refer to the following section :

- <a href="fg_revenuecat.html" target="_blank">Revenuecat</a>
- <a href="fg_adjust_purchase_tracker.html" target="_blank">Adjust Purchase Tracker</a>

# Remote Config

To access FG RemoteConfig Scripting API and documentation :

- <a href="fg_remote_config.html" target="_blank">RemoteConfig</a>

To add an implementation of FG RemoteConfig please refer to the corresponding section :

- <a href="fg_tn_remote_config.html" target="_blank">TapNation Remote Config</a>

To use TapNation RemoteConfig for A/B testing features based on network or other ua parameters, you will need to
integrate the FG TNRemoteConfigNetworkAttribution plugin :

- <a href="fg_tn_remote_config_network_attribution.html" target="_blank">TapNation Remote Config Network Attribution</a>

# Notifications

To access FG Notifications Scripting API and documentation :

- <a href="fg_notifications.html" target="_blank">Notifications</a>

To add an implementation of FG Notifications please refer to the corresponding section :

- <a href="fg_unity_notifications.html" target="_blank">Unity Notifications</a>

# Tools

To get infos about the tools and utils available in FunGames SDK, please refer to the following section :

- <a href="fg_tools.html" target="_blank">Tools</a>

# Introduction

FunGames SDK is a Unity toolkit we use at TapNation to help game developers monitoring their app growth.

It's a modular package, that aims to easily deploy multiple in-house products or external plugins into your app,
providing a simple scripting interface. It is mainly useful to control:

- **Analytics**: Real-time Analytics, App Behavior Tracking, Performance Monitoring, etc.
- **Monetization**: Ad Integration, In-App Purchases, Cross Promotion
- **User Consent**: Data Collection Preferences, Privacy Management (ATT, GDPR, CCPA, etc.)
- **User Acquisition**: Marketing and Advertising campaigns, User Attribution
- **Remote Config**: Dynamic Configuration, A/B Testing
- **Notifications**: In-App Notifications, Push Notifications

> **<ins>Setup your App page on Store</ins>**
>
>When releasing the app with ad keys, please use **Tap Nation website** as a developer contact website on **Apple Store** and **Google Play** for a proper app-ads.txt file scanning.

# Download SDK

There are several packages you can integrate, depending on your situation :

## Light Package (CPI Test)

If installing FG for the first time to perform **CPI test**, please follow the **PDF guide** and download the **Unity
package** from this [link](https://drive.google.com/drive/folders/1pT9kdasnr9DT2fVHBLI-EmQYZ--VwzrO?usp=sharing).

## Start Package (SoftLaunch, Monet test, other...)

To start **full integration of FG SDK**, please follow the [Integration steps](#integration-steps).

## Custom Package (Migrate from previous version)

If **migrating to FG 3.3+** please **reach out our SDK support team**, we will make sure to send you the **proper
package** corresponding to your **current project setup**.


> **Note :** To do so, please provide the **list of all FG modules** you currently have installed in your project. In Unity, you can find it in the **FunGames Integration Manager**.
> You can also install [_this package_](https://drive.google.com/file/d/11bqJ0P5StviXV12I00iGOFVtHoez2C-8/view?usp=sharing) and, from the Unity Editor toolbar, go to _FunGames > Generate Config File_. This will create a csv file containing your current FG setup.


**Before you install new version**, you must **remove _Assets/FunGames_ folder** from your project. Keep FG settings and
prefab assets.

**After installing new version**, click the **"Add all Prefabs and Settings"** button in **FunGames Integration
Manager** to fill up your scene with the new required components and create missing Settings assets.

You can now **delete FunGamesSDK prefab** from your scene, and **put all the FG prefabs inside the "FunGames"
GameObject**. (**Adjust** and **GameAnalytics** prefabs should be kept **outside**)

> **Note :** FG 3.3 is up to date with some recent versions of other SDKs, if you are using an older version of a given SDK, you'll need to update it manually to a newest version. Latest compatible version of each SDK can be found in _Assets/FunGames_Externals_ folder.

# Integration Steps

To get started with FunGames SDK, follow these steps :

- **Import**
  the [FunGamesSDK plugin](https://drive.google.com/file/d/1MpeS-59qlPdwgSPjbkJWHGQO3FEHLuuZ/view?usp=sharing) into your
  Unity project

- Install **iOS 14 Advertising Support** from the Package Manager.

- In **FunGames Integration Manager**, click the **"Prefabs and Settings"** button to fill up your scene with the
  required components and create Settings assets.

![](_source/prefab_and_settings.png)

- From Unity Editor, in _Assets/Resource/FunGames_, fill the Settings assets with the credentials that our Publishing
  team provides.

You are now ready to go ! FG will automatically initialize its modules and trigger callbacks events. Without any
additional setup, FG will display User Consent flow when required, and start collecting core Analytics data.

To ensure proper project setup, get your app [prepared for build](#prepare-for-build) and check the **Scripting API**
section of each **module documentation** to start using all FG features.

# Prepare for Build

Before building your app from Unity, please follow the instructions below :

## Android

### Target API level

Since 31th August 2021, Google Play Store requires all new apps to target at least Android 14 (API level 34).

To build properly with Unity 2022.*, we also recommend to use set the minimum API level to 24.

### Gradle version

To build successfully with Unity < v2022.x and latest Google Libraries, we recommend using version 6.7.1 of gradle :

- Download it [here](https://gradle.org/next-steps/?version=6.7.1&format=bin).
- Extract gradle-6.7.1 folder somewhere in your workspace.
- in Unity menu : _Edit > Preferences... > External Tools_, change the Gradle path with new version installed.

### Player settings

In _Player Settings > Other Settings_ make sure:

- Scripting Backend is set to "IL2CPP"
- _API Compatibility Level_ is set to ".NET Framework".

In _Player Settings > Publishing Settings_, enable gradle files as follow:

![](_source/fg_core_player_settings.png)

> **<ins>Migrating to Unity 2022+</ins>**
>
> When migrating to Unity 2022+ from a previous version, you might need to delete the AndroidManifest and Gradle files in the _Assets/Plugins/Android_ folder. By re-enabling it in Player Settings, Unity will generate new ones with the proper format.

## iOS

To ensure proper build on iOS, always make sure you have the latest version of Xcode, cocoapods and MacOS to avoid
potential incompatibility issues.

# Scripting API

## Attributes

FunGamesSDK interface provide the following information :

```csharp
FunGamesSDK.IsInitialized;          \\ Is FunGames SDK initialization completed ?
FunGamesSDK.IsConnectedToInternet   \\ Is the device connected to Internet
FunGamesSDK.IsFirstConnection;      \\ Is user first session ?
FunGamesSDK.CurrentSessionNumber;   \\ Number of the current session (starting from 1)
FunGamesSDK.DaysSinceFirstCo;       \\ Days since first session
FunGamesSDK.DaysSinceLastCo;        \\ Days since last session
```

## Callbacks

FunGamesSDK automatically triggers some event callbacks you can subscribe to attach custom actions :

```csharp
void Awake()
{
    FunGamesSDK.Callbacks.Initialization += OnInitialization; // Triggered when module initialization start
    FunGamesSDK.Callbacks.OnInitialized += OnInitialized; // Triggered when module initialization is completed
    FunGamesSDK.Callbacks.OnAdsRemoved += OnAdsRemoved; // Triggered after ads were removed by RemoveAds()
}

private void OnInitialization()
{
    // Do something when module start initializing.
}

private void OnInitialized(bool success)
{
    // Do something when module initialization completed.
}

private void OnAdsRemoved()
{
    // Do something when ads are removed.
}

private void OnDestroy()
{
    FunGamesSDK.Callbacks.Initialization -= OnInitialization;
    FunGamesSDK.Callbacks.OnInitialized -= OnInitialized;
    FunGamesSDK.Callbacks.OnAdsRemoved -= OnAdsRemoved;
}
```

## Remove Ads

Use ```FunGamesSDK.RemoveAds()``` to remove all ads from your app. You can also set your own _RemoveAds_ config as
follow :

```csharp
// Remove all ads except REWARDED
FGAdType[] adsToRemove =
{
    FGAdType.Banner,
    FGAdType.Interstitial,
    FGAdType.AppOpen,
    FGAdType.MREC,
    FGAdType.OFFLINE,
    FGAdType.AUDIOADS,
    FGAdType.INGAMEADS,
    FGAdType.SPONSORED,
    FGAdType.CROSSPROMO
};
FunGamesSDK.RemoveAds(adsToRemove);
```

## Manual initialization [Beta]

> **Note :**  When using manual initialization, please make sure to **test carefully your integration**.
> If any questions, feel free to **reach out our SDK support team**.


When running FunGamesSDK, all FG prefabs in the scene will start automatically to initialize according to our standard
flow. If you want to change this behavior or control manually the initialization of 1 or multiple FG modules please
use _FGModuleInitializer_ as follow :

1) Create a FGModuleInitializer object and add the corresponding instances of the modules you want to manually
   initialize.

Example:

```csharp
private void Awake()
{
    FGModuleInitializer fgModuleInitializer = new FGModuleInitializer();
    fgModuleInitializer.AddModule(FGMediationManager.Instance);
    fgModuleInitializer.AddModule(FGAmazonWithMax.Instance);
    // etc.
}
```

> **⚠️️ <ins>Attention</ins>**
>
> The Awake() function must happens before the init flow of FG. To do so, execution order of the above script must be **≤ -43**.
>
><ins>Example:</ins> You can set the execution order by script using ```[DefaultExecutionOrder(-43)]```

2) Once created, call ```fgModuleInitializer.ForceInit()``` whenever you want to start the corresponding modules
   initialisation.

Example:

```csharp
[DefaultExecutionOrder(-43)]
public class YourCustomScript : MonoBehaviour
{
    private FGModuleInitializer fgModuleInitializer;

    private void Awake()
    {
        fgModuleInitializer = new FGModuleInitializer();
        fgModuleInitializer.AddModule(FGMax.Instance);
        fgModuleInitializer.AddModule(FGAmazonWithMax.Instance);
    }

    private void Start()
    {
        // Start initializing modules after 10 seconds
        StartCoroutine(ForceInitAfterSeconds(10));
    }

    private IEnumerator ForceInitAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        fgModuleInitializer.ForceInit();
    }
}
```

# TROUBLESHOOTING

## Duplicate class error (Android)

When your app contains references to different versions of the same library, a duplicate class error might be thrown
during build. To fix this issue, you can exclude the conflicting library from the build, by deleting the lower version
from your project.

Example:

```
Execution failed for task ':launcher:checkReleaseDuplicateClasses'.
> A failure occurred while executing com.android.build.gradle.internal.tasks.CheckDuplicatesRunnable
   > Duplicate class com.google.android.gms.actions.ItemListIntents found in modules jetified-play-services-basement-17.0.0-runtime (:play-services-basement-17.0.0:) and jetified-play-services-basement-18.3.0-runtime (com.google.android.gms:play-services-basement:18.3.0)
```

In this case, you'll need to delete the lower version (17.0.0) of ```play-services-basement``` from your project.

## Duplicate symbols error (iOS)

When building from XCode, if you encounter an error of type ```ld: 8 duplicate symbols``` , add ```-ld64``` to the
```Other Linker Flags``` in the ```Build Settings``` of your target (UnityFramework and Unity-iPhone).